#include "BloomFilter.h"
#include "HashMap.h"
#include "Hash.h"
#include <stdbool.h>
#include <string.h>
#include <errno.h>

void printBloomFilter(BloomFilter *bf) {
  printf("BloomFilter: ");
  for (int i = 0; i < bf->num_bits; i++) {
    printf("%d", GETBIT(bf->data, i));
    if (i % 8 == 7) printf(" ");  // Print out in blocks of 8 bits
  }
  printf("\n");
}

int get_str(char *buf) {
	char *res = fgets(buf,MAX_STRING_LEN,stdin);
	if (res == NULL) {
		return -1;
	} else {
		return 0;
	}
}

int get_value(int *value) {
	char *endptr = NULL;
	int retval;
	errno = -1;


	while (errno != 0) {
		char *buf = (char *) calloc(MAX_STRING_LEN,sizeof(char));
		errno = 0;
		printf("value: ");
		retval = get_str(buf);
		if (retval == -1) {
			return retval;
		}
		retval = (int) strtol(buf,&endptr,10);
		*value = retval;
		free(buf);
	}
	return 0;
}

int main() {
	char *buf = (char *) calloc(MAX_STRING_LEN,sizeof(char));
	int value;
	int value2;
	int errmark = 0;

	HashFunc funs[] = {hash_first_letter, hash_xor, hash_djb2,hash_sdbm,hash_lose_lose,hash_jenkins_otat,hash_murmur,hash_superfast};
	BloomFilter *bf = newBloomFilter(100,5,funs);

	while (errmark != -1) {
		printf("input 2-letter function symbol: ");
		errmark = get_str(buf);
		if (strncmp(buf,"af",1) == 0) {
			//add
			printf("key: ");
			errmark = get_str(buf);
			if (errmark != -1) {
				BloomFilter_Add(bf,buf);
			}
			printf("inserted\nkey:%s\n",buf);
		} else if (strncmp(buf,"pf",2) == 0) {
			printBloomFilter(bf);
		} else if (strncmp(buf,"cf",2) == 0) {
			//check filter
			printf("key: ");
			errmark = get_str(buf);
			int res = 0;
			if (errmark != -1) {
				res = BloomFilter_Check(bf,buf);
			}
			if (res == 1) {
				puts("Found key");
			} else {
				puts("did not finds key");
			}
		} else if (strncmp(buf,"nf",2) == 0) {
			//newHashMap
			errmark = get_value(&value);
			if (errmark != -1) {
				errmark = get_value(&value2);
				if (errmark != -1) {
					bf = newBloomFilter(value,value2,funs);
				}
			}
		}
		buf[0] = '\0';
		buf[1] = '\0';
	}
	puts("\nbye");
}
