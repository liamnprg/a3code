#include "HashMap.h"
#include <stdbool.h>
#include <string.h>
#include <errno.h>

// @brief Prints out the hashmap to hel debug
void printHashMap(HashMap *hm) {
  printf("--- HashMap Contents ---\n");
  for (int i = 0; i < hm->num_buckets; i++) {
    printf("* Bucket #%d: \n", i);
    if (hm->buckets[i] == NULL) printf("    - NULL\n");
    for (BucketNode *cur = hm->buckets[i]; cur != NULL; cur = cur->next) {
      printf("    - KEY: '%s', VALUE: '%s'\n", cur->key, cur->value);
    }
  }
}

int get_str(char *buf) {
	char *res = fgets(buf,MAX_STRING_LEN,stdin);
	if (res == NULL) {
		return -1;
	} else {
		return 0;
	}
}

int get_value(int *value) {
	char *endptr = NULL;
	int retval;
	errno = -1;


	while (errno != 0) {
		char *buf = (char *) calloc(MAX_STRING_LEN,sizeof(char));
		errno = 0;
		printf("value: ");
		retval = get_str(buf);
		if (retval == -1) {
			return retval;
		}
		retval = (int) strtol(buf,&endptr,10);
		*value = retval;
		free(buf);
	}
	return 0;
}

int main() {
	char *buf = (char *) calloc(MAX_STRING_LEN,sizeof(char));
	char *buf2 = (char *) calloc(MAX_STRING_LEN,sizeof(char));
	int value=0;
	int value2=0;
	int errmark = 0;

	HashMap *hm = newHashMap(1,hash_murmur);

	while (errmark != -1) {
		printf("input 2-letter function symbol: ");
		errmark = get_str(buf);
		if (strncmp(buf,"ad",1) == 0) {
			//add
			printf("key: ");
			errmark = get_str(buf);
			printf("val: ");
			errmark = get_str(buf2);
			if (errmark != -1) {
				hm = HashMap_Add(hm,buf,buf2);
			}
			printf("inserted\nkey:%s\nval:%s\n",buf,buf2);
		} else if (strncmp(buf,"pm",2) == 0) {
			printHashMap(hm);
		} else if (strncmp(buf,"de",2) == 0) {
			//delete
			printf("key: ");
			errmark = get_str(buf);
			if (errmark != -1) {
				hm = HashMap_Delete(hm,buf);
			}
			printf("deleted\nkey:%s\n",buf);

		} else if (strncmp(buf,"nh",2) == 0) {
			//newHashMap
			HashFunc funs[] = {hash_first_letter, hash_xor, hash_djb2,hash_sdbm,hash_lose_lose,hash_jenkins_otat,hash_murmur,hash_superfast};
			errmark = get_value(&value);
			if (errmark != -1) {
				errmark = get_value(&value2);
				if (errmark != -1) {
					if (value2 < 8) {
						HashMap_Free(hm);
						hm = newHashMap(value,funs[value2]);
					}
				}
			}
		}
		buf[0] = '\0';
		buf[1] = '\0';
	}
	HashMap_Free(hm);
	puts("\nbye");
}
