/**
 *                         Dynamic Hash Maps
 *                              ---
 *  Like Dynamic Arrays, we can also have Dynamic HashMaps. Working out the
 *  math would for the complexity for this structure would give you
 *  amortized expected O(1) for Add(), Find() and Delete(). Try to prove
 *  this if just computing expected and amortized costs on their own wasn't
 *  enough for you! :)
 *                              ---
 *
 * This is the file where all of your implementation for the HashMap goes. 
 * You will be submitting this file (along with BloomFilter.c) to MarkUs.
 *
 * TestHashMap.c provides some test cases to test your implementation. 
 * You can compile it using the Makefile and running the executable produced.
 *
 * CSCB63 Winter 2022 - Coding Assignment 3
 * (c) Mustafa Quraish
 */

#include "HashMap.h"

// @brief Create and initialize a new HashMap with the given values
//
// @param num_buckets  The number of buckers the HashMap starts off with
// @param func         The hash function to be used for this HashMap
//
// @return             A pointer to the HashMap
//
// @TODO: Implement this function
HashMap *newHashMap(int num_buckets, HashFunc func) {
	HashMap *hm = calloc(1,sizeof(HashMap));
	if (hm == NULL) {
		puts("ENOMEM");
		exit(1);
	} 
	BucketNode **bucketArr = calloc(num_buckets,sizeof(BucketNode *));
	/*for (int i = 0; i<num_buckets; i++) {
		BucketNode *bucket = calloc(1,sizeof(BucketNode));
		if (bucket == NULL) {
			puts("ENOMEM");
			exit(1);
		} 
		bucketArr[i] = bucket;
	}*/

	hm->num_buckets = num_buckets;
	hm->num_elems = 0;
	hm->buckets = bucketArr;
	hm->func = func;

	return hm;
}

// @brief Construct a new HashMap of the given size, and re-hash all
//        the values from the old one. This needs to be done when expanding
//        or shrinking the Dynamic HashMap during insertion and deletion.
//
// @param hm         The old HashMap
// @param new_size   Required size of new HashMap
//
// @return           New HashMap (old one must be freed).
//
// @TODO: Implement this function. For the sake of consistency, the automarker
//        will expect you to re-hash all the elements in a specific order. Go
//        through the buckets sequentially from [0 - hm->num_buckets], and for
//        each one insert the nodes in the linked list sequentially. See the
//        test casesfor example behaviour.
HashMap *HashMap_Resize(HashMap *hm, int new_size) {
	HashMap *nhm = newHashMap(new_size,hm->func);

	int i = 0;
	while (i < hm->num_buckets)  {
		for (BucketNode *cur_bkt = hm->buckets[i]; cur_bkt != NULL; cur_bkt=cur_bkt->next) {
			HashMap_Add(nhm,cur_bkt->key,cur_bkt->value);
		}
		i++;
	}
	HashMap_Free(hm);
	return nhm;
}

// @brief Add a key-value pair to the HashMap. Dynamically resize
//        the hashmap when at capacity.
//
// @param hm     HashMap
// @param key    Key to be inserted
// @param value  Corresponding Value
//
// @return     Pointer to resulting HashMap (useful when you expand)
//
// @TODO: Implement this function. Here are some notes in implementation:
//        (1) If the key exists, replace the value.
//
//         (2) If the key doesn't exist, add the new BucketNode at the
//            *head* of the corresponding linked list
//
//        (3) To get the array into the buckets[] array, use the hash
//            function inside `hm` and then take that value mod the
//            number of buckets:
//                      idx = hash(key) % num_buckets
//
//        (4) AFTER you have inserted the new element, if the number
//            of elements in the HashMap is >= the number of buckets,
//            double the size of the HashMap and re-hash all the elements.
HashMap *HashMap_Add(HashMap *hm, const char *key, const char *value) {
	if (hm == NULL) {
		return NULL;
	}
	int num_buckets = hm->num_buckets;
	//off-by-one bug here?
	hash_t hkey = hm->func(key) % num_buckets;


	BucketNode *found = NULL;
	for (BucketNode *cur = hm->buckets[hkey]; cur!=NULL; cur=cur->next) {
		if (strncmp(cur->key,key,MAX_STRING_LEN) == 0) {
			found = cur;
		}
	}

	if (found != NULL) {
		//case: Modify old key
		memcpy(&found->value,value,MAX_STRING_LEN);
	} else {
		//case: Insert new Key
		BucketNode *bkt_old = hm->buckets[hkey];
		BucketNode *bkt_new = calloc(1,sizeof(BucketNode));
		if (bkt_new == NULL) {
			puts("ENOMEM");
			exit(1);
		}


		memcpy(&bkt_new->key, key, MAX_STRING_LEN);
		memcpy(&bkt_new->value, value, MAX_STRING_LEN);
		bkt_new->next = bkt_old;

		hm->buckets[hkey] = bkt_new;
		hm->num_elems=hm->num_elems + 1;
	}

	if (hm->num_elems >= hm->num_buckets) {
		hm = HashMap_Resize(hm,2*hm->num_elems);
	}

	

	return hm;
}

// @brief Find the value of the given key in the HashMap
//        Return NULL if key does not exist.
//
// @param hm   HashMap
// @param key  Key to be queried in the hashmap
//
// @return     The corresponding value to the key, or NULL.
//
// @TODO: Implement this function
char *HashMap_Find(HashMap *hm, const char *key) {
	hash_t hkey = hm->func(key) % hm->num_buckets;
	BucketNode *cur_bkt = hm->buckets[hkey];

	while (cur_bkt != NULL && strncmp(cur_bkt->key,key,MAX_STRING_LEN) != 0) {
		cur_bkt=cur_bkt->next;
	}
	if (cur_bkt == NULL) {
		return NULL;
	} else {
		return cur_bkt->value;
	}
}

// @brief Delete a key-value pair from the HashMap. Dynamically resize
//        the HashMap if necessary.
//
// @param hm   HashMap
// @param key  Key for which the pair needs to be deleted.
//
// @return     Pointer to resulting HashMap (useful when you shrink)
//
// @TODO: Implement this function. Here are some notes in implementation:
//        (1) If the key doesn't exist, don't do anything.
//
//         (2) Don't reorder the rest of the list when removing the node
//
//         (3) AFTER deleting the element, if num_elems < num_buckets/4, then
//             shrink the HashMap by half. (Similar to Dynamic Arrays). You'll
//             need to re-hash all the elements.
HashMap *HashMap_Delete(HashMap *hm, const char *key) {
	//strategy: Find node, Delete the node, set node->next to current
	//resize after
	hash_t hkey = hm->func(key) % hm->num_buckets;

	BucketNode *cur_item = hm->buckets[hkey];
	BucketNode *prev = NULL;

	//Find Node
	while (cur_item != NULL && strncmp(cur_item->key,key,MAX_STRING_LEN) != 0) {
		prev = cur_item;
		cur_item=cur_item->next;
	}

	//Delete node;
	if (cur_item != NULL) {
		//prev-cur->next
		if (prev != NULL) {
			prev->next = cur_item->next;

		//cur->next
		} else {
			hm->buckets[hkey] = cur_item->next;
		}
		hm->num_elems-=1;
		free(cur_item);
	}
	//resize;
	if (hm->num_elems < hm->num_buckets/4) {
		hm = HashMap_Resize(hm, hm->num_buckets/2);
	}
	return hm;
}

// @brief Frees all the memory used by the HashMap
//
// @param hm   HashMap
//
// @TODO: Implement this function. You will need to use valgrind to properly 
//        check the functionality.
void HashMap_Free(HashMap *hm) {
	//HashMap *hm = calloc(1,sizeof(HashMap));
	//BucketNode **bucketArr = calloc(num_buckets,sizeof(BucketNode *));
	//BucketNode *bkt_new = calloc(1,sizeof(BucketNode)); <-- for each BucketNode
	for (int i = 0; i < hm->num_buckets; i++) {
		BucketNode *next = NULL;
		for (BucketNode *cur_bkt=hm->buckets[i]; cur_bkt != NULL; cur_bkt=next) {
			next = cur_bkt->next;
			free(cur_bkt);
		}
	}
	free(hm->buckets);
	free(hm);
	return;
}
